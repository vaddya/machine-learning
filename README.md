# [Machine Learning](https://www.coursera.org/learn/machine-learning) 
### by Stanford University (Andrew Ng)

![Machine Learning](https://d3njjcbhbojbot.cloudfront.net/api/utilities/v1/imageproxy/https://coursera.s3.amazonaws.com/topics/ml/large-icon.png)

1. Linear Regression
2. Logistic Regression
3. Multi-class Classification and Neural Networks
4. Neural Network Learning
5. Regularized Linear Regression and Bias/Variance
6. Support Vector Machines
7. K-Means Clustering and Principal Component Analysis
8. Anomaly Detection and Recommender Systems